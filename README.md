## Annonces importantes

- Merci de commenter [cette issue](https://gitlab.com/m2105/projet/issues/26) avec votre nom si ce n'est pas déjà fait. 
- Merci de mettre une photo sur le profil de votre compte gitlab.
- Vous pouvez poster des issues pour poser une question ou partager une ressource/idée intéressante.
-  Le "design" du site ne compte pas. **Ceux sont les fonctionnalités et la qualité du code qui sont importantes**.




## Projet

Vous venez d’intégrer l’équipe informatique d’un célèbre IUT de la Réunion. 
Actuellement, le prêt de matériel du département Réseau et Télécommunications est 
géré manuellement. Comme premier projet, l’IUT vous demande de remplacer ce 
système par un site web dynamique. 

Le site Web devra permettre de gérer le matériel de prêt disponible dans le 
département R&T. Cela concerne tout ce qui pourrait être emprunté, telles que les 
machines (serveurs, switch, Rpi, Arduino…), les pièces (clavier, souris, câbles 
Ethernet…), les objets (tournevis, rallonge…), ou mêmes des licences informatiques. 
Le responsable du matériel pourra ajouter ou supprimer un objet dans la liste, ou bien le 
rendre indisponible (ex : En réparation). 

Les utilisateurs pourront visualiser la liste du matériel disponible, puis faire une 
demande d’emprunt directe auprès du responsable. Le responsable sélectionnera 
l’emprunteur dans l’outil et l’objet sera alors indisponible. Au retour de l’objet emprunté, 
le responsable libèrera l’objet. 

Dans un deuxième temps, les utilisateurs pourront faire des demandes de réservations 
directement dans l’outil pour s’assurer de la disponibilité d’un objet à une période 
précise. La recherche d’un matériel pourra se faire en fonction du matériel disponible 
dans la base. 

Une fois cette première partie opérationnelle, vous pourrez améliorer votre outil en 
ajoutant de nouvelles fonctionnalités telles que de créer des alertes lorsque les délais de 
retour sont dépassés, générer des statistiques d’utilisation pour identifier les matériels 
très demandés, ou bien imaginer vous-même de nouvelles fonctions. 
 

#### Etapes du travail à réaliser : 
---

1. A partir du cahier des charges, faire une première ébauche de la structure du 
site : quelles pages ? quels formulaires ? quels script PHP appeler quand on valide 
un formulaire ? quel traitement pour quel résultat à l’affichage ? 
2. Ensuite, identifier toutes les informations nécessaires au fonctionnement du site 
pour concevoir la base de données. Créer la base et valider les requêtes SQL qui 
seront utilisées plus tard dans les scripts. 
3. Ecrire les premières pages HTML et les tester grâce au fichier fourni debug.php. 
Intégrer une première feuille de style basique. 
4. Lorsque les pages HTML sont fonctionnelles, écrire les scripts PHP. Procéder en 
deux temps : commencer par traiter les données reçues des formulaires, et 
ensuite requêter la BDD pour affichage. 
5. Gérer la sécurité en différenciant l’accès en fonction des profils utilisateurs. 
6. Tester, débugger, améliorer le rendu et ajouter des fonctionnalités. 


#### Consignes et barèmes : 
---

Vous devrez faire appel aux connaissances en programmation acquises durant toute 
l’année, mais aussi être capable d’acquérir de nouvelles connaissances de manière 
autonome pour répondre aux besoins de votre projet.  
Vous n’êtes pas autorisé à utiliser de framework, et devrez être capable de justifier la 
présence de chaque ligne de code du projet. 
Toutes vos productions devront être stockées sur GitLab qui contiendra un readme. 
 
Vous tiendrez aussi un journal de bord dans lequel vous décrirez brièvement au fur et à 
mesure vos activités pendant et hors des séances de TP. 
 
L’évaluation du projet se fera sur la base du contenu de votre Gitlab ainsi qu’une 
présentation orale de 5 min (lors du TP6) au cours de laquelle vous présenterez à votre 
responsable de TP : 
o Le fonctionnement de votre outil 
o Le code source 
o Le contenu de votre GitLab  
 
La gestion de vos sources représente 3 à 4 points de la note finale : 
- Organisation du Gitlab  
- Code indenté et commenté 
- Régularités des commits commentés 
- Tenue du journal de bord 
 
 
#### Planning prévisionnel : 
---

Afin de vous aider à planifier votre travail, voici une liste de tâches (non exhaustives) 
que vous allez devoir mettre en œuvre : 

##### TP1 : "Projet is Coming"  

- Création de la structure du site : Pages web, formulaires, fonctionnement, 
affichage du résultat… 
- Conception de la BDD et tests des premières requêtes SQL 
- Plus de détails : [project is coming](https://gitlab.com/m2105/projet/blob/master/notes/s02e01.md)

##### TP2 : 

- Pages statiques et formulaires  
- Utilisation d’une feuille de style basique 
- Amélioration de la BDD et tests des dernières requêtes SQL 
- Plus de détails : [the project's road](https://gitlab.com/m2105/projet/blob/master/notes/s02e02.md)

##### TP3 :  

- Script PHP de traitement de formulaire et affichage HTML 
- Script PHP avec requêtes SQL SELECT et affichage HTML 

##### TP4 : 

- Script PHP avec requêtes SQL SELECT / DELETE / INSERT INTO / UPDATE 

##### TP5 :  

- Gestion des accès utilisateurs  
- Formulaire HTML dynamique, les menus déroulants sont peuplés par des 
résultats de requêtes SQL. Les formulaires sont générés par des scripts PHP 
 
##### TP6 – Présentation du travail 

- Amélioration de la feuille de d’une feuille de style 
- Debug  
