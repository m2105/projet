# the mountain and the project

## Quatrième séance.
Vous avez fini votre projet, en tout cas je l'espère: 
- Vous avez les statistiques étudiants, admin, etc...
- Vous avez du javascript dans votre code (cochez les checkbox par exemple)
- Vous avez fait vos CSS pour avoir des pages dignes de ce nom.


## Si vous êtes rapides
- Vous avez fait du refactoring de votre code (fonctions, fichiers, etc.)
- Vous avez des stats plus poussées et un présentation avec des graphiques etc.

## Pour vous aider (ou pas)
- Il faut ecrire du code
- IL faut ecrire beaucoup de code.
