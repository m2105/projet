# project is coming


## Première séance.
Durant cette première séance vous devez avoir:
- Créé vote dépôt git. (1 minute)
- Effectué votre premier commit correspondant au moins à votre journal de bord. (5 minutes)
- Votre dépôt doit être structuré (une arborescence correcte, etc...). (8 minutes)
- Commenté l'[issue de présentation](https://gitlab.com/m2105/projet/issues/26). (1 minute)
- Vous devez avoir "une idée" de l'articulation de votre site. (Le nombre de page, et ce qu'elles contiennent, ...). Si possible avec des images intégré a votre journal de bord. (60 minutes)
- Vous avez la structure de votre base de données (60 minutes)
- Vous avez rempli votre base de données avec des fausses données (15 minutes)
- Vous avez créée une page php qui liste tout le matériel (30 minutes) -test de connection  à la base et requete select-

Ainsi, votre dépôt et votre journal de bord doit contenir les points ci-dessus avec quelques explications.

## Si vous êtes rapides
- Vous avez divisé votre projet en issue/tâches/milestones. (au moins dans votre tête)
- Vous avez toutes vos pages html/php avec lien entre les pages
- Vous avez identifié les points qui vous prendront du temps.
- Vous avez commencé a écrire la documentation sur la base de donnée (les champs, leur utilisation, et vous avez un commit de toutes les commandes sql)

## Pour vous aider
- KISS 
    - Keep It Simple and Stupid
    - Faites un structure de site simple (minimiser les pages) et une base de données simple (minimiser les tables)
- Important
    - Anticipez (ou pas) la partie réservation
    - Dès la conception pensez à la réservation ou n'y pensez pas, mais prenez une décision dès le départ.
- Attention, pour votre bien ...
    - N'inventez des fonctionnalités qui ne sont pas demandées/évaluées.

## La solution du prof (pas forcément bonne) 
- 3 (voire 2) tables suffisent. Matériel, Utilisateurs, Historique (optionnelle)
- 6 pages php suffisent. index.php, login.php, index_admin.php, emprunt.php, retour.php, resa.php
- L'interconnexion: index.php liste le matériel, directement (avec option de classement), un lien vers login si administrateur
- login.php envoi vers index_admin.php. Cette derinère page liste de matériel et permet d'emprunter, ou retourner
- Depuis la page index.php, il y a un lien réserver pour l'utilisateur (il faut peut etre un login utilisateur aussi... à voir)
 